import React from 'react';

import ErrorBoundary from 'features/common/components/ErrorBoundary';

const withErrorBoundary = (Component) =>
  function (props) {
    return (
      <ErrorBoundary>
        <Component {...props} />
      </ErrorBoundary>
    );
  };

export default withErrorBoundary;
