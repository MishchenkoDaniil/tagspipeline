import { createSlice } from '@reduxjs/toolkit';
import ROLES from 'features/common/enums';
import * as authThunks from 'features/common/thunks/auth';
import * as profileThunks from 'features/profile/thunks/profile';

import { setToken, removeToken } from 'shared/lib/localStorage';

const slice = createSlice({
  name: 'auth',
  initialState: {
    user: null,
    token: null,
    role: ROLES.Seller,
    isAuthenticated: false,
    isAttempted: false,
  },
  reducers: {
    logout: (state) => {
      state.user = null;
      state.token = null;
      state.isAuthenticated = false;
      state.isAttempted = true;
      removeToken();
    },
    setLogin: (state, action) => {
      state.user = action.payload.user;
      state.token = action.payload.token;
      state.isAuthenticated = true;
      state.isAttempted = true;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(authThunks.authThunk.fulfilled, (state, { payload: { data } }) => {
      const { user, token } = data;
      state.user = user;
      state.token = token;
      state.isAuthenticated = true;
      state.isAttempted = true;
      state.role = user.role;
      setToken(token);
    });
    builder.addCase(
      authThunks.userPreferencesThunk.fulfilled,
      (state, { payload: { data, token } }) => {
        const { user } = data;
        state.isAuthenticated = true;
        state.isAttempted = true;
        state.token = token;
        state.user = user;
        state.role = user.role
      }
    );
    builder.addCase(profileThunks.uploadAvatarThunk.fulfilled, (state, { payload: { data } }) => {
      if (state.user) {
        state.user.photo = data.url;
      }
    });
    builder.addCase(profileThunks.deleteAvatarThunk.fulfilled, (state) => {
      state.user.photo = null;
    });
    builder.addCase(profileThunks.updateProfileThunk.fulfilled, (state, { payload: { data } }) => {
      state.user = data.user;
    });
    builder.addCase(
      profileThunks.updateSellerProfileThunk.fulfilled,
      (state, { payload: { data } }) => {
        state.user = { ...state.user, ...data };
      }
    );
    builder.addCase(
      profileThunks.updateBuyerProfileThunk.fulfilled,
      (state, { payload: { data } }) => {
        state.user.profile = data;
      }
    );
  },
});

export const { logout, setLogin } = slice.actions;

export default slice.reducer;

export const selectCurrentUser = (state) => state.auth.user;
export const selectCurrentUserRole = (state) => state?.auth.role;
export const selectIsAuthenticated = (state) => state.auth.isAuthenticated;
