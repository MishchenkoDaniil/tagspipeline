import { useSelector } from 'react-redux';
import { Navigate } from 'react-router-dom';
import PropTypes from 'prop-types';
import { selectCurrentUserRole } from 'features/common/slices/auth';
import ROLES from 'features/common/enums';
import paths from 'app/Routes/paths';

import withPrivateRoute from 'features/common/hocs/withPrivateRoute';

const SellerRoute = withPrivateRoute(({ component: RouteComponent }) => {
  const role = useSelector(selectCurrentUserRole);

  if (role === ROLES.Seller) {
    return <RouteComponent />;
  }

  return <Navigate to={paths.buyer()} />;
});

SellerRoute.displayName = 'SellerRoute';
SellerRoute.propTypes = {
  component: PropTypes.oneOfType([PropTypes.func, PropTypes.object]).isRequired,
};

export default SellerRoute;
