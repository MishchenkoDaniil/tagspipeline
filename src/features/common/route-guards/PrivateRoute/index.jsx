import { useSelector } from 'react-redux';
import { Navigate } from 'react-router-dom';
import PropTypes from 'prop-types';
import { selectIsAuthenticated } from 'features/common/slices/auth';
import paths from 'app/Routes/paths';

function PrivateRoute({ component: RouteComponent }) {
  const isAuthenticated = useSelector(selectIsAuthenticated);

  if (isAuthenticated) {
    return <RouteComponent />;
  }

  return <Navigate to={paths.signIn()} />;
}

PrivateRoute.displayName = 'PrivateRoute';
PrivateRoute.propTypes = {
  component: PropTypes.oneOfType([PropTypes.func, PropTypes.object]).isRequired,
};
export default PrivateRoute;
