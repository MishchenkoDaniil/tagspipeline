import classNames from 'classnames/bind';

import paths from 'app/Routes/paths';
import FuseNavigation from '@fuse/core/FuseNavigation/FuseNavigation';
import styles from './styles.module.css';

const cx = classNames.bind(styles);

const navigationConfig = [
  {
    id: 'dashboards',
    title: 'Dashboard',
    type: 'item',
    url: paths.dashboard(),
  },
  {
    id: 'listings',
    title: 'My Listings',
    type: 'collapse',
    open: true,
    children: [
      {
        id: 'currentListings',
        title: 'Current Listings',
        type: 'item',
        url: paths.currentListings(),
      },
      {
        id: 'addListing',
        title: 'Add Listing',
        type: 'item',
        url: paths.addListing(),
      },
      {
        id: 'propostions',
        title: 'Propostions',
        type: 'item',
        url: paths.propostions(),
      },
    ],
  },
  {
    id: 'Portfolio',
    title: 'Portfolio',
    type: 'collapse',
    open: true,
    children: [
      {
        id: 'activeContracts',
        title: 'Active Contracts',
        type: 'item',
        url: paths.activeContractsSeller(),
      },
      {
        id: 'currentBids',
        title: 'Current Bids',
        type: 'item',
        url: paths.currentBids(),
      },
    ],
  },
];

function SellerDrawerList() {
  return (
    <FuseNavigation
      layout="vertical"
      className={cx('seller-drawer-list')}
      navigation={navigationConfig}
    />
  );
}

SellerDrawerList.displayName = 'SellerDrawerList';

SellerDrawerList.propTypes = {};

export default SellerDrawerList;
