import classNames from 'classnames/bind';

import paths from 'app/Routes/paths';

import FuseNavigation from '@fuse/core/FuseNavigation/FuseNavigation';
import styles from './styles.module.css';

const cx = classNames.bind(styles);

const navigationConfig = [
  {
    id: 'dashboard',
    title: 'Dashboard',
    type: 'item',
    url: paths.dashboardBuyer(),
  },
  {
    id: 'Property',
    title: 'Property',
    type: 'collapse',
    open: true,
    children: [
      {
        id: 'overview',
        title: 'Overview',
        type: 'item',
        url: paths.overview(),
      },
      {
        id: 'searchListings',
        title: 'Search Listings',
        type: 'item',
        url: paths.searchListings(),
      },
      {
        id: 'suggestedListings',
        title: 'Suggested Listings',
        type: 'item',
        url: paths.suggestedListings(),
      },
      {
        id: 'watchList',
        title: 'Watchlist',
        type: 'item',
        url: paths.watchList(),
      },
      {
        id: 'preferences',
        title: 'Preferences',
        type: 'item',
        url: paths.preferences(),
      },
    ],
  },
  {
    id: 'Contracts',
    title: 'Contracts',
    type: 'collapse',
    open: true,
    children: [
      {
        id: 'contractsOverview',
        title: 'Overview',
        type: 'item',
        url: paths.contractsOverview(),
      },
      {
        id: 'activeContracts',
        title: 'Active Overview',
        type: 'item',
        url: paths.activeContractsBuyer(),
      },
      {
        id: 'portfolioHistory',
        title: 'Portfolio History',
        type: 'item',
        url: paths.portfolioHistory(),
      },
      {
        id: 'currentBids',
        title: 'Bid History',
        type: 'item',
        url: paths.currentBids(),
      },
    ],
  },
  {
    id: 'Clients',
    title: 'Clients',
    type: 'collapse',
    open: true,
    children: [
      {
        id: 'clientList',
        title: 'Client List',
        type: 'item',
        url: paths.clientList(),
      },
    ],
  },
];

function BuyerDrawerList() {
  return (
    <FuseNavigation
      layout="vertical"
      className={cx('buyer-drawer-list')}
      navigation={navigationConfig}
    />
  );
}

BuyerDrawerList.displayName = 'BuyerDrawerList';

BuyerDrawerList.propTypes = {};

export default BuyerDrawerList;
