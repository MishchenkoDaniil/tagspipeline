import PropTypes from 'prop-types';
import HomeTemplate from 'features/common/templates/HomeTemplate';
import Header from 'features/common/components/Header';
import { withOpenDrawer } from 'features/common/hocs/withOpenDrawer';
import AdminDrawerList from 'features/common/components/AdminDrawerList';

const AdminLayout = withOpenDrawer(({ children, open, onOpenDrawer, onCloseDrawer }) => (
  <HomeTemplate
    open={open}
    onCloseDrawer={onCloseDrawer}
    onOpenDrawer={onOpenDrawer}
    DrawerChildren={<AdminDrawerList />}
    HeaderChildren={<Header open={open} onDrawerOpen={onOpenDrawer} />}
  >
    {children}
  </HomeTemplate>
));

AdminLayout.displayName = 'AdminLayout';

AdminLayout.propTypes = {
  children: PropTypes.node.isRequired,
};

export default AdminLayout;
