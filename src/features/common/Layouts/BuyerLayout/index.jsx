import PropTypes from 'prop-types';
import HomeTemplate from 'features/common/templates/HomeTemplate';
import Header from 'features/common/components/Header';
import { withOpenDrawer } from 'features/common/hocs/withOpenDrawer';
import BuyerDrawerList from 'features/common/components/BuyerDrawerList';

const BuyerLayout = withOpenDrawer(({ children, open, onOpenDrawer, onCloseDrawer }) => (
  <HomeTemplate
    open={open}
    onCloseDrawer={onCloseDrawer}
    onOpenDrawer={onOpenDrawer}
    DrawerChildren={<BuyerDrawerList />}
    HeaderChildren={<Header open={open} onDrawerOpen={onOpenDrawer} />}
  >
    {children}
  </HomeTemplate>
));

BuyerLayout.displayName = 'BuyerLayout';

BuyerLayout.propTypes = {
  children: PropTypes.node.isRequired,
};

export default BuyerLayout;
