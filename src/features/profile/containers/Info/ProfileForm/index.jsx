import { useEffect } from 'react';
import { useFormik } from 'formik';
import classNames from 'classnames/bind';
import { TextField, Button, Box, Grid } from '@mui/material';
import PropTypes from 'prop-types';
import * as Yup from 'yup';
import styles from './styles.module.css';

const cx = classNames.bind(styles);

const validationSchema = Yup.object().shape({
  first_name: Yup.string().required('First name is required'),
  last_name: Yup.string().required('Last name is required'),
  email: Yup.string().email('Enter a valid email').required('Email is required'),
});

function ProfileForm({ onSubmit, data }) {
  const formik = useFormik({
    initialValues: {
      first_name: '',
      last_name: '',
      email: '',
    },
    validationSchema,
    validateOnMount: false,
    validateOnChange: false,
    onSubmit,
  });

  useEffect(() => {
    if (data) {
      formik.setValues({
        first_name: data.first_name,
        last_name: data.last_name,
        email: data.email || '',
      });
    }
  }, [data]);

  return (
    <Box container>
      <form onSubmit={formik.handleSubmit} className={cx('form')}>
        <Grid
          container
          direction="column"
          justifyContent="flex-start"
          alignItems="flex-start"
          spacing={1}
        >
          <Grid item>
            <TextField
              sx={{ width: '400px' }}
              name="first_name"
              label="First name"
              error={formik.errors.first_name && Boolean(formik.errors.first_name)}
              helperText={formik.touched.first_name && formik.errors.first_name}
              onChange={formik.handleChange}
              value={formik.values.first_name}
            />
          </Grid>
          <Grid item>
            <TextField
              sx={{ width: '400px' }}
              name="last_name"
              label="Last name"
              error={formik.errors.last_name && Boolean(formik.errors.last_name)}
              helperText={formik.touched.last_name && formik.errors.last_name}
              onChange={formik.handleChange}
              value={formik.values.last_name}
            />
          </Grid>
          <Grid item>
            <TextField
              sx={{ width: '400px' }}
              name="email"
              label="Email"
              type="email"
              error={formik.errors.email && Boolean(formik.errors.email)}
              helperText={formik.touched.email && formik.errors.email}
              onChange={formik.handleChange}
              value={formik.values.email}
            />
          </Grid>
          <Grid item>
            <div className="flex justify-between items-center">
              <Button variant="contained" type="submit">
                Save
              </Button>
            </div>
          </Grid>
        </Grid>
      </form>
    </Box>
  );
}

ProfileForm.displayName = 'ProfileForm';

ProfileForm.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  data: PropTypes.objectOf(PropTypes.object).isRequired,
};

export default ProfileForm;
