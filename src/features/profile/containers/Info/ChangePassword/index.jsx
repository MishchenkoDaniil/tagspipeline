import { useFormik } from 'formik';
import classNames from 'classnames/bind';
import { TextField, Button, Box, Grid } from '@mui/material';
import PropTypes from 'prop-types';
import * as Yup from 'yup';
import styles from './styles.module.css';

const cx = classNames.bind(styles);

const validationSchema = Yup.object().shape({
  old_password: Yup.string('Enter your password')
    .min(8, 'Password should be of minimum 8 characters length')
    .required('Password is required'),
  password: Yup.string('Enter your password')
    .min(8, 'Password should be of minimum 8 characters length')
    .required('Password is required'),
  confirmPassword: Yup.string('Enter your password')
    .min(8)
    .oneOf([Yup.ref('password'), null], 'Passwords must match')
    .required('Required')
    .trim(),
});

function ChangePasswordForm({ onSubmit }) {
  const formik = useFormik({
    initialValues: {
      old_password: '',
      password: '',
      confirmPassword: '',
    },
    validationSchema,
    validateOnMount: false,
    validateOnChange: false,
    onSubmit: (values) => {
      onSubmit({
        old_password: values.old_password,
        password: values.password,
      });
    },
  });

  return (
    <Box container>
      <form onSubmit={formik.handleSubmit} className={cx('form')}>
        <Grid
          container
          direction="column"
          justifyContent="flex-start"
          alignItems="flex-start"
          spacing={1}
        >
          <Grid item>
            <TextField
              sx={{ width: '400px' }}
              name="old_password"
              label="Old Password"
              type="password"
              error={formik.errors.old_password && Boolean(formik.errors.old_password)}
              helperText={formik.touched.old_password && formik.errors.old_password}
              onChange={formik.handleChange}
              value={formik.values.old_password}
            />
          </Grid>
          <Grid item>
            <TextField
              sx={{ width: '400px' }}
              name="password"
              label="Password"
              type="password"
              error={formik.errors.password && Boolean(formik.errors.password)}
              helperText={formik.touched.password && formik.errors.password}
              onChange={formik.handleChange}
              value={formik.values.password}
            />
          </Grid>
          <Grid item>
            <TextField
              sx={{ width: '400px' }}
              name="confirmPassword"
              label="Confirm password"
              type="password"
              error={formik.errors.confirmPassword && Boolean(formik.errors.confirmPassword)}
              helperText={formik.touched.confirmPassword && formik.errors.confirmPassword}
              onChange={formik.handleChange}
              value={formik.values.confirmPassword}
            />
          </Grid>
          <Grid item>
            <div className="flex justify-between items-center">
              <Button variant="contained" type="submit">
                Save
              </Button>
            </div>
          </Grid>
        </Grid>
      </form>
    </Box>
  );
}

ChangePasswordForm.displayName = 'ChangePasswordForm';

ChangePasswordForm.propTypes = {
  onSubmit: PropTypes.func.isRequired,
};

export default ChangePasswordForm;
