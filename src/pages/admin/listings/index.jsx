import loadable from '@loadable/component';
import Loading from 'features/common/components/Loading';

const ListingsPage = loadable(() => import(/* webpackChunkName: "buyer-page" */ './page'), {
  resolveComponent: ({ ListingsPage: Page }) => Page,
  fallback: <Loading />,
});

export default ListingsPage;
