import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';

/* eslint-disable import/prefer-default-export */
export function ListingsPage() {
    return <div>Listings</div>
}

ListingsPage.displayName = 'ListingsPage';
