import loadable from '@loadable/component';
import Loading from 'features/common/components/Loading';
import 'react-gallery-carousel/dist/index.css';

const ListingDetailsPage = loadable(() => import(/* webpackChunkName: "seller-page" */ './page'), {
  resolveComponent: ({ ListingDetailsPage: Page }) => Page,
  fallback: <Loading />,
});

export default ListingDetailsPage;
