import { Box, Typography } from '@mui/material';
import paths from 'app/Routes/paths';
import Loading from 'features/common/components/Loading/index';
import { checkIfIsLoading } from 'features/common/slices/api/index';
import {
  createListingThunk,
  getListingByIdThunk,
  updateListingThunk,
} from 'features/common/thunks/listing';
import { useFormik } from 'formik';
import _ from 'lodash';
import { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate, useParams } from 'react-router-dom';
import LISTING_STATUSES from 'src/constants/listingStatuses';
import * as Yup from 'yup';
import AddListingForm from './components/addListingForm/index';
import AddListingTerms from './components/addListingTerms/index';
import ListingVerification from './components/ListingVerification/index';

const validationForNumericFields = (name) => {
  return Yup.number()
    .required(`${name} is required`)
    .typeError('Must be numeric')
    .positive('Must be positive');
};

const validationSchema = (step) =>
  step === 0
    ? Yup.object().shape({
        listing: Yup.object().shape({
          cost: validationForNumericFields('Cost'),
          marketPrice: validationForNumericFields('Market price'),
          currentPrice: validationForNumericFields('Current price'),
          askingPrice: validationForNumericFields('Asking price'),
          optionPrice: validationForNumericFields('Option price'),
          propertyInformation: Yup.string('Enter information about property').required(
            'Property information is required'
          ),
          propertySize: validationForNumericFields('Property size'),
          bedrooms: validationForNumericFields('Number of bedrooms'),
          biddingEnds: Yup.date('Enter bidding date')
            .required('Bidding ends field is required')
            .typeError('Must be date format'),
          bathrooms: validationForNumericFields('Number of bathrooms'),
          garages: validationForNumericFields('Number of garages'),
          gardenSize: validationForNumericFields('Garden size'),
          terraceSize: validationForNumericFields('Terrace size'),
          yearBuilt: validationForNumericFields('Year built'),
          postalCode: validationForNumericFields('Postal code'),
        }),
        address: Yup.object().shape({
          lineOne: Yup.string('Enter Address Line 1')
            .required('Address Line 1 is required')
            .min(3, 'Must be at least 3 characters'),
          lineTwo: Yup.string('Enter Address Line 2')
            .nullable()
            .min(3, 'Must be at least 3 characters'),
          city: Yup.string('Enter city').required('City is required'),
          state: Yup.string('Enter state').required('State is required'),
          zipCode: Yup.string('Enter zip code').required('Zip code is required'),
        }),
      })
    : Yup.object().shape({
        contract: Yup.object().shape({
          price: validationForNumericFields('Price'),
          fullName: Yup.string('Enter full name').required('Full name is required'),
          date: Yup.date('Enter date')
            .required('Date is required')
            .typeError('Must be date format'),
        }),
      });

/* eslint-disable import/prefer-default-export */
export function AddListingPage() {
  const [step, setStep] = useState(0);
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const params = useParams();
  const isLoading = useSelector(checkIfIsLoading);
  const [editMode, setEditMode] = useState(false);

  useEffect(async () => {
    if (params.listingId) {
      setEditMode(true);
      const response = await dispatch(getListingByIdThunk(params.listingId));
      if (response.payload.success) {
        const data = _.mapKeys(response.payload.data, (value, key) => _.camelCase(key));
        const { photos, address, ...listingData } = data;
        const { id, isFavorite, bookmark, ...listing } = listingData;
        formik.setFieldValue('listing', listing);
        formik.setFieldValue(
          'address',
          _.mapKeys(address, (value, key) => _.camelCase(key))
        );
        if (!_.isEmpty(photos)) {
          formik.setFieldValue(
            'photos',
            photos.map((file) => ({ dataURL: file.file }))
          );
        }
      }
    } else {
      setEditMode(false);
      formik.resetForm();
    }
  }, [params]);

  const onSubmit = async (values) => {
    if (step === 0) {
      if (editMode) {
        delete values.contract;
        if (!values.address.lineTwo) delete values.address.lineTwo;
        const data = {
          ...values,
          photos: values.photos.map((file) => file.dataURL),
        };
        const response = await dispatch(updateListingThunk({ listingId: params.listingId, data }));
        if (response.payload.success) {
          navigate(paths.currentListings());
        }
      } else {
        switchToNextStep();
      }
    } else if (step === 1) {
      if (!values.address.lineTwo) delete values.address.lineTwo;
      const data = {
        ...values,
        photos: values.photos.map((file) => file.dataURL),
      };
      const response = await dispatch(createListingThunk(data));
      if (response.payload.success) {
        switchToNextStep();
      }
    }
  };

  const formik = useFormik({
    initialValues: {
      listing: {
        cost: 0,
        marketPrice: 0,
        currentPrice: 0,
        askingPrice: 0,
        optionPrice: 0,
        biddingEnds: new Date(),
        propertySize: 0,
        bedrooms: 0,
        bathrooms: 0,
        yearBuilt: new Date().getFullYear(),
        postalCode: '',
        gardenSize: 0,
        terraceSize: 0,
        garages: 0,
        propertyInformation: '',
        status: LISTING_STATUSES.AVAILABLE_STATUS,
      },
      address: {
        lineOne: '',
        lineTwo: '',
        city: '',
        state: '',
        zipCode: '',
      },
      contract: {
        price: 0,
        fullName: '',
        date: new Date(),
      },
      photos: [],
    },
    onSubmit,
    validationSchema: () => Yup.lazy(() => validationSchema(step)),
    validateOnMount: false,
    validateOnChange: false,
  });

  const switchToNextStep = () => {
    setStep(step + 1);
  };

  const switchToPreviousStep = () => {
    setStep(step - 1);
  };

  const buttons = [
    [
      {
        type: 'submit',
        text: editMode ? 'Save' : 'Next',
        withLoader: editMode
      },
    ],
    [
      {
        type: 'button',
        text: 'Back',
        onClick: switchToPreviousStep,
      },
      {
        type: 'submit',
        text: 'Next',
        withLoader: true,
      },
    ],
    [
      {
        type: 'button',
        text: 'Back to dashboard',
        onClick: () => navigate(paths.dashboard()),
      },
    ],
  ];

  return (
    <div>
      {isLoading && <Loading />}
      <div>
        <Typography variant="h3">{editMode ? 'Edit listing' : 'Add listing'}</Typography>
        <Box sx={{ mt: 4 }}>
          {step === 0 ? (
            <AddListingForm buttons={buttons[step]} form={formik} />
          ) : step === 1 ? (
            <AddListingTerms buttons={buttons[step]} form={formik} />
          ) : (
            <ListingVerification buttons={buttons[step]} />
          )}
        </Box>
      </div>
    </div>
  );
}

AddListingPage.displayName = 'AddListingPage';
