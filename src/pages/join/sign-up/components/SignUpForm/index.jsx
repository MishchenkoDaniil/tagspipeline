import { Button, FormControl, FormLabel, Radio, RadioGroup, TextField } from '@mui/material';
import FormControlLabel from '@mui/material/FormControlLabel';
import classNames from 'classnames/bind';
import ROLES from 'features/common/enums';
import capitalize from 'lodash/capitalize';
import PropTypes from 'prop-types';
import styles from './styles.module.css';

const cx = classNames.bind(styles);

function SignUpForm({ form }) {
  return (
    <form onSubmit={form.handleSubmit} className={cx('form')}>
      <div>
        <FormControl component="fieldset">
          <FormLabel component="legend">Choose Role</FormLabel>
          <RadioGroup
            row
            onChange={(e) => form.setFieldValue('user.role', e.currentTarget.value)}
            aria-label="role"
            value={form.values.user.role}
            name="user.role"
          >
            <FormControlLabel
              value={ROLES.Buyer}
              control={<Radio />}
              label={capitalize(ROLES.Buyer)}
            />
            <FormControlLabel
              value={ROLES.Seller}
              control={<Radio />}
              label={capitalize(ROLES.Seller)}
            />
          </RadioGroup>
        </FormControl>
      </div>
      <TextField
        name="user.firstName"
        label="First Name"
        type="text"
        error={form.errors.user?.firstName && Boolean(form.errors.user?.firstName)}
        helperText={form.touched.user?.firstName && form.errors.user?.firstName}
        onChange={form.handleChange}
        value={String(form.values.user.firstName)}
      />
      <TextField
        name="user.lastName"
        label="Last Name"
        type="text"
        error={form.errors.user?.lastName && Boolean(form.errors.user?.lastName)}
        helperText={form.touched.user?.lastName && form.errors.user?.lastName}
        onChange={form.handleChange}
        value={String(form.values.user.lastName)}
      />
      <TextField
        name="user.email"
        label="Email"
        type="email"
        error={form.errors.user?.email && Boolean(form.errors.user?.email)}
        helperText={form.touched.user?.email && form.errors.user?.email}
        onChange={form.handleChange}
        value={String(form.values.user.email)}
      />
      <TextField
        name="user.password"
        label="Password"
        type="password"
        error={form.errors.user?.password && Boolean(form.errors.user?.password)}
        helperText={form.touched.user?.password && form.errors.user?.password}
        onChange={form.handleChange}
        value={form.values.user.password}
      />
      <TextField
        name="user.passwordConfirmation"
        label="Confirm password"
        type="password"
        error={
          form.errors.user?.passwordConfirmation && Boolean(form.errors.user?.passwordConfirmation)
        }
        helperText={
          form.touched.user?.passwordConfirmation && form.errors.user?.passwordConfirmation
        }
        onChange={form.handleChange}
        value={form.values.user.passwordConfirmation}
      />
      <div className="flex justify-end">
        <Button color="secondary" variant="contained" type="submit">
          Sign Up
        </Button>
      </div>
    </form>
  );
}

SignUpForm.displayName = 'SignUpForm';

SignUpForm.propTypes = {
  form: PropTypes.objectOf(PropTypes.object).isRequired,
};

export default SignUpForm;
