const ACCOUNT_TYPES = {
  SAVING: 'savings',
  CHECKING: 'checking',
};

export default ACCOUNT_TYPES;
