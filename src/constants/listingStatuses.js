const LISTING_STATUSES = {
    AVAILABLE_STATUS: 'available',
    NOT_AVAILABLE_STATUS: 'not available'
};

export default LISTING_STATUSES