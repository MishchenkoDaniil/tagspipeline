import { useCallback, useEffect } from 'react';
import { logout } from 'features/common/slices/auth';
import { userPreferencesThunk } from 'features/common/thunks/auth';
import { useSelector, useDispatch } from 'react-redux';
import { getToken } from 'shared/lib/localStorage';
import Loading from 'features/common/components/Loading';

const withAuth = (Component) =>
  function ({ ...props }) {
    const dispatch = useDispatch();
    const { isLoading, isAttempted } = useSelector((state) => state.auth);

    const handleFetchPreferences = useCallback(async (token) => {
      await dispatch(userPreferencesThunk(token));
    }, []);

    useEffect(() => {
      if (!isAttempted) {
        const token = getToken();
        if (!token) {
          dispatch(logout());
        } else {
          handleFetchPreferences(token);
        }
      }
    }, [handleFetchPreferences, isAttempted]);

    if (isLoading) {
      return <Loading />;
    }
    return <Component {...props} />;
  };

export default withAuth;
