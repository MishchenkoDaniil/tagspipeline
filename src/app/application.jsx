import { hot } from 'react-hot-loader/root';
import compose from 'compose-function';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import withErrorBoundary from 'features/common/hocs/withErrorBoundary';

import withProviders from 'app/providers';
import Routes from 'app/Routes';

const enhance = compose(hot, withErrorBoundary, withProviders);

export function Application() {
  return (
    <>
      <Routes />
      <ToastContainer />
    </>
  );
}

export default enhance(Application);
